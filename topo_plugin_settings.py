# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 16:10:42 2015

@author: lucadelu
"""

from PyQt4.QtCore import QSettings
from PyQt4.QtGui import QComboBox, QLineEdit, QCheckBox
from types import StringType, UnicodeType
import inspect
import re
from qgis.utils import iface
from qgis.gui import QgsMessageBar
from qgis.core import QgsMessageLog


class GTCSettings:
    """
    Class to save and to restore settings to QSettings

    GTCSettings.saveWidgetsValue(ui, QSettings("QGIS", "grass_topology_checker"), toolName)
    GTCSettings.restoreWidgetsValue(ui, QSettings("QGIS", "grass_topology_checker"), toolName)
    """
    s = QSettings("QGIS", "grass_topology_checker")

    @staticmethod
    def _check(string):
        """Check the type of string

        :param obj string: a string, it should be as UnicodeType or StringType
        """
        if isinstance(string, UnicodeType) or isinstance(string, StringType):
            return str(string)
        else:
            return str("")

    @staticmethod
    def saveWidgetsValue(ui, tool=""):
        """Save the parameters used into a tool in the GTCSettings

        :param obj ui: the obkect of tool's UI
        :param str tool: the name of the tool
        """
        if tool:
            tool = re.sub(r"[^\w\s]", '', tool)
            tool = re.sub(r"\s+", '_', tool)

        for name, obj in inspect.getmembers(ui):
            if isinstance(obj, QComboBox):
                name = obj.objectName()
                index = obj.currentIndex()
                text = obj.itemText(index)
                GTCSettings.setValue(tool + "/" + name, text)

            if isinstance(obj, QLineEdit):
                name = obj.objectName()
                value = obj.text()
                GTCSettings.setValue(tool + "/" + name, value)

            if isinstance(obj, QCheckBox):
                name = obj.objectName()
                state = obj.isChecked()
                GTCSettings.setValue(tool + "/" + name, state)

    @staticmethod
    def restoreWidgetsValue(ui, tool=""):
        """Restore the parameters used earlier into a tool in the GTCSettings

        :param obj ui: the obkect of tool's UI
        :param str tool: the name of the tool
        """
        if tool:
            tool = re.sub(r"[^\w\s]", '', tool)
            tool = re.sub(r"\s+", '_', tool)

        if tool not in GTCSettings.s.childGroups():
            return

        for name, obj in inspect.getmembers(ui):
            if isinstance(obj, QComboBox):
                index = obj.currentIndex()
                name = obj.objectName()

                value = GTCSettings.value(tool + "/" + name, "", unicode)
                if value == "":
                    continue

                index = obj.findText(value)
                if index == -1:
                    continue
                else:
                    obj.setCurrentIndex(index)

            if isinstance(obj, QLineEdit):
                name = obj.objectName()
                value = GTCSettings.value(tool + "/" + name, "", unicode)
                if value:
                    obj.setText(value)

            if isinstance(obj, QCheckBox):
                name = obj.objectName()
                value = GTCSettings.value(tool + "/" + name, True, bool)
                if value is not None:
                    obj.setChecked(value)

    @staticmethod
    def setValue(key, value):
        """Set the value for the given key

        :param str key: the key to set
        :param str value: the value for the given key
        """
        return GTCSettings.s.setValue(key, value)

    @staticmethod
    def value(key, default=None, type=None):
        """Return the value of the given key

        :param str key: the key to get
        :param str default: the default value to return
        :param obj type: the type of value
        """
        if default and type:
            return GTCSettings.s.value(key, default, type=type)
        elif default and not type:
            return GTCSettings.s.value(key, default)
        else:
            return GTCSettings.s.value(key)

    @staticmethod
    def allKeys():
        """Return all keys of GTCSettings"""
        return GTCSettings.s.allKeys()

    @staticmethod
    def saveToFile(fil):
        groups = []
        fil.write("[General]\n")
        for k in GTCSettings.s.childKeys():
            fil.write("{ke}={va}\n".format(ke=k, va=GTCSettings.s.value(k)))
        fil.write("\n")
        for k in GTCSettings.allKeys():
            if k.find('/') != -1:
                key = k.split('/')
                if key[0] not in groups:
                    fil.write("\n[{ke}]\n".format(ke=key[0]))
                    groups.append(key[0])
                fil.write("{ke}={va}\n".format(ke=key[1],
                                               va=GTCSettings.s.value(k)))


class MessageHandler:
    """
    Handler of message notification via QgsMessageBar to display
    non-blocking messages to the user.

    MessageHandler.[information, warning, critical, success](title, text, timeout)
    MessageHandler.[information, warning, critical, success](title, text)
    MessageHandler.error(message)
    """

    messageLevel = [QgsMessageBar.INFO,
                    QgsMessageBar.WARNING,
                    QgsMessageBar.CRITICAL]

    # SUCCESS was introduced in 2.7
    # if it throws an AttributeError INFO will be used
    try:
        messageLevel.append(QgsMessageBar.SUCCESS)
    except:
        pass
    try:
        messageTime = iface.messageTimeout()
    except:
        pass

    @staticmethod
    def information(title="", text="", timeout=0):
        """Function used to display an information message

        :param str title: the title of message
        :param str text: the text of message
        :param int timeout: timeout duration of message in seconds
        """
        level = MessageHandler.messageLevel[0]
        if timeout:
            timeout = MessageHandler.messageTime
        MessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def warning(title="", text="", timeout=0):
        """Function used to display a warning message

        :param str title: the title of message
        :param str text: the text of message
        :param int timeout: timeout duration of message in seconds
        """
        level = MessageHandler.messageLevel[1]
        if timeout:
            timeout = MessageHandler.messageTime
        MessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def critical(title="", text="", timeout=0):
        """Function used to display a critical message

        :param str title: the title of message
        :param str text: the text of message
        :param int timeout: timeout duration of message in seconds
        """
        level = MessageHandler.messageLevel[2]
        if timeout:
            timeout = MessageHandler.messageTime
        MessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def success(title="", text="", timeout=0):
        """Function used to display a succeded message

        :param str title: the title of message
        :param str text: the text of message
        :param int timeout: timeout duration of message in seconds
        """
        # SUCCESS was introduced in 2.7
        # if it throws an AttributeError INFO will be used
        try:
            level = MessageHandler.messageLevel[3]
        except:
            level = MessageHandler.messageLevel[0]
        if timeout:
            timeout = MessageHandler.messageTime
        MessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def error(message):
        """Function used to display an error message

        :param str message: the text of message
        """
        # TODO: add an action to message bar to trigger the Log Messages Viewer
        # button = QToolButton()
        # action = QAction(button)
        # action.setText("Apri finestra dei logs")
        # button.setCursor(Qt.PointingHandCursor)
        # button.setStyleSheet("background-color: rgba(255, 255, 255, 0); color: black; "
        #                   "text-decoration: underline;")
        # button.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
        # button.addAction(action)
        # button.setDefaultAction(action)
        # level = MessageHandler.messageLevel[1]
        # messageBarItem = QgsMessageBarItem(u"Error", u"Un errore è avvenuto, controllare i messaggi di log",
        #                                 button, level, 0, iface.messageBar())
        #
        # iface.messageBar().pushItem(messageBarItem)

        MessageHandler.warning("GRASS topological check",
                               "Error! Check QGIS message log", 0)
        QgsMessageLog.logMessage(message, "Python Error")

    @staticmethod
    def messageBar(title, text, level, timeout):
        if title:
            try:
                iface.messageBar().pushMessage(title.decode('utf-8'),
                                               text.decode('utf-8'), level,
                                               timeout)
            except Exception:
                iface.messageBar().pushMessage(str(title), str(text), level,
                                               timeout)
        else:
            try:
                iface.messageBar().pushMessage(text.decode('utf-8'), level,
                                               timeout)
            except Exception:
                iface.messageBar().pushMessage(str(text), level,
                                               timeout)
