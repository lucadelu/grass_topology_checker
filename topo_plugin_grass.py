# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 11:12:47 2015

@author: lucadelu
"""
import os
import sys
import subprocess
from topo_plugin_settings import MessageHandler, GTCSettings
PIPE = subprocess.PIPE


def readfile(path):
    """Read the file and return a string

    :param str path: the path to a file
    """
    f = open(path, 'r')
    s = f.read()
    f.close()
    return s


def writefile(path, s):
    """Write the file from a string, new line should be added in the string

    :param str path: the path to a file
    :param str s: the string to write into file
    """
    f = open(path, 'w')
    f.write(s)
    f.close()

from PyQt4.QtCore import *

class gtcGRASS():
    """The class to use GRASS GIS as backend of GrassTopologicalCheck plugin"""
    def __init__(self):
        self.mapset = None
        self.mapsetpath = None
        self.grassbin = str(GTCSettings.value("grassbin", ""))
        self.grassdatabase = str(GTCSettings.value("grassdata", ""))
        self.location = str(GTCSettings.value("grasslocation", ""))
        self.epsg = str(GTCSettings.value("epsgcode", ""))
        self._initialize()
        self.outputs = []
        self.vinogr = 'new'
        self.vbuild = 'overlap'
        self.vclean = 'wronglinearea'
        self.vcat = 'multi'

    def _removeMapset(self):
        """Remove mapset with all the contained data"""
        import shutil
        shutil.rmtree(self.mapsetpath)

    def _initialize(self):
        """Inizialize GRASS GIS"""
        pid = os.getpid()
        startcmd = [self.grassbin, '--config', 'path']
        p = subprocess.Popen(startcmd, shell=False, stdin=PIPE,
                             stdout=PIPE, stderr=PIPE)
        out, err = p.communicate()

        if (p.returncode == 0 and err) or p.returncode != 0:
            raise Exception("Error running GRASS: Missing parameters in settings")
        if sys.platform.startswith('linux'):
            gisbase = out.strip()
        elif sys.platform.startswith('win'):
            if out.find("OSGEO4W home is") != -1:
                gisbase = out.strip().splitlines()[1]
            else:
                gisbase = out.strip()
            os.environ['GRASS_SH'] = os.path.join(gisbase, 'msys', 'bin',
                                                  'sh.exe')
        # Set GISBASE environment variable
        os.environ['GISBASE'] = gisbase
        # define GRASS-Python environment
        gpydir = os.path.join(gisbase, "etc", "python")
        sys.path.append(gpydir)

        # DATA
        # define GRASS DATABASE
        # Set GISDBASE environment variable
        os.environ['GISDBASE'] = self.grassdatabase
        os.environ['PATH'] += os.pathsep + os.path.join(gisbase, 'extrabin')

        self.mapset = 'gtc_{pid}'.format(pid=pid)
        locexist = os.path.join(self.grassdatabase, self.location)
        if not os.path.exists(locexist):
            if not self.epsg:
                raise Exception("Error running GRASS: ",
                                "EPSG code missing in settings")
            startcmd = self.grassbin + ' -c EPSG:' + self.epsg + ' -e ' + locexist
            p = subprocess.Popen(startcmd, shell=True, stdin=PIPE,
                                 stdout=PIPE, stderr=PIPE)
            out, err = p.communicate()
            if p.returncode != 0:
                raise Exception("Error running GRASS: ",
                                "Failed to create temporary location")
        self.mapsetpath = os.path.join(self.grassdatabase, self.location,
                                       self.mapset)
        if not os.path.exists(self.mapsetpath):
            os.mkdir(self.mapsetpath)
        wind = readfile(os.path.join(self.grassdatabase, self.location,
                                     "PERMANENT", "DEFAULT_WIND"))
        writefile(os.path.join(self.mapsetpath, "WIND"), wind)

        path = os.getenv('LD_LIBRARY_PATH')
        dirlib = os.path.join(gisbase, 'lib')
        if path:
            path = dirlib + os.pathsep + path
        else:
            path = dirlib
        os.environ['LD_LIBRARY_PATH'] = path

        # language
        os.environ['LANG'] = 'en_US'
        os.environ['LOCALE'] = 'C'
        # launch session
        import grass.script.setup as gsetup
        gsetup.init(gisbase, self.grassdatabase, self.location, self.mapset)
        if 'GRASS_PROJSHARE' not in os.environ.keys():
            os.environ['GRASS_PROJSHARE'] = 'C:\OSGeo4W\share\proj'

        if 'GRASS_PYTHON' not in os.environ.keys():
            os.environ['GRASS_PYTHON'] = 'C:\OSGeo4W\bin\python.exe'

        if 'SHELL' not in os.environ.keys():
            os.environ['SHELL'] = 'C:\Windows\system32\cmd.exe'
        import grass.script.core as gcore
        gcore.os.environ['GRASS_OVERWRITE'] = '1'
        return 0

    def gimport(self, inp, snap, overwrite, outlog):
        """Import data into GRASS database

        :param str inp: the path to source data
        :param str snap: the snap value to use in v.in.org
        :param bool overwrite: set overwrite flag
        :param str outlog: path to the output log
        """
        import grass.script.core as gcore
        name = None
        if inp.startswith('PG'):
            conn = inp.split()
            for item in conn:
                if 'table' in item:
                    name = item.split('=')[1]
                    self.intemp = name.replace("'", "")
        else:
            self.intemp = os.path.split(inp)[-1].split('.')[0]
        if self.intemp[0].isdigit():
            self.intemp = "a_{inp}".format(inp=self.intemp)
        self.intemp = self.intemp.replace("-", "_")
        self.inouttemp = "{pre}_checked".format(pre=self.intemp)
        if name:
            inp = inp.replace(" table={inp}".format(inp=name), "")
            inp = inp.replace("'", "")
        com = ['v.in.ogr', 'input={i}'.format(i=inp),
               'output={o}'.format(o=self.inouttemp),
               'snap={sn}'.format(sn=snap)]
        if inp.startswith('PG'):
            com.append("layer={na}".format(na=self.intemp))
        if overwrite:
            com.append('-o')
        runcom = gcore.Popen(com, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = runcom.communicate()
        if runcom.returncode != 0:
            raise Exception("Error running GRASS: "
                            "Error running v.in.ogr {e}".format(e=err))
        self.outputs.append(self.inouttemp)
        err = err.replace('\x08', '')
        with open(outlog, 'w') as outfile:
            outfile.write('#### V.IN.OGR output ####\n')
            outfile.write(err)
            if 'polygons got lost during import' in err:
                MessageHandler.warning('Some polygons got lost during import')
            if 'degenerated polygon' in err:
                MessageHandler.warning('Some polygons was not valid')
                outfile.write('#### INVALID POLYGONS ####')
                for line in err.splitlines():
                    if 'degenerated polygon' in line:
                        outfile.write(line.split(':')[1].strip().replace('(','').replace(')',''))
            if 'additional areas' in err:
                MessageHandler.warning('New areas were created')
                outfile.write('#### NEW AREAS ####')
                for line in err.splitlines():
                    if 'additional areas' in line:
                        outfile.write(line.strip())
            if 'stored as category in layer 2' in err:
                MessageHandler.warning('Some polygons overlap in input layer')
                self.outputs.append('overlap')
        outfile.close()

    def run_build(self):
        """Run v.build to obtain the error map"""
        import grass.script.core as gcore
        self.outbuild = "{ma}_build".format(ma=self.intemp)
        runcom = gcore.Popen(['v.build', 'map={i}'.format(i=self.inouttemp),
                              'error={o}'.format(o=self.outbuild),
                              'option=build'],
                             stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = runcom.communicate()
        if runcom.returncode != 0:
            raise Exception("Error running GRASS: "
                            "Error running v.build {e}".format(e=err))
        self.outputs.append(self.outbuild)

    def run_clean(self, outlog,  tool='rmline'):
        """Run v.clean to obtain the error map

        :param str tool: the tools to use in v.clean
        :param str outlog: path to the output log
        """
        import grass.script.core as gcore
        self.outclean = "{ma}_clean_{to}".format(ma=self.intemp, to=tool)
        self.outcleanerr = "{ma}_clean_{to}_error".format(ma=self.intemp,
                                                          to=tool)
        runcom = gcore.Popen(['v.clean', 'input={i}'.format(i=self.inouttemp),
                              'output={i}'.format(i=self.outclean),
                              'error={o}'.format(o=self.outcleanerr),
                              'tool={to}'.format(to=tool)],
                             stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = runcom.communicate()
        with open(outlog, 'w') as outfile:
            outfile.write('#### V.CLEAN output ####\n')
            outfile.write(err)
        outfile.close()
        if runcom.returncode != 0:
            raise Exception("Error running GRASS: "
                            "Error running v.clean {e}".format(e=err))
        self.outputs.append(self.outclean)
        self.outputs.append(self.outcleanerr)

    def run_category(self, inp=None, opt="print", out=None):
        """Run v.category and return

        :param str inp: the name of GRASS vector to use, if None it use self.intemp
        :param str opt: the option to use in v.category, the possible
                        options are "print", "report" and "add"
        :param str out: the name of the output vector file
        """
        if not inp:
            inp = self.inouttemp
        if opt not in ['print', 'report',  'add']:
            raise Exception("Option can only be print, report or add")

        import grass.script.core as gcore
        cmd = ['v.category', 'option={op}'.format(op=opt), 'layer=1',
               'input={i}'.format(i=inp)]
        if out:
            cmd.append("output={ou}".format(ou=out))
        if opt == 'report':
            cmd.append('-g')
        runcom = gcore.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = runcom.communicate()

        if runcom.returncode != 0:
            raise Exception("Error running GRASS: "
                            "Error running v.category {e}".format(e=err))
        if opt == 'print':
            return out.replace('\n','/').split('/')
        else:
            return out

    def run_extract(self, cats):
        """Extract elements using cats list

        :param list cats: a list with cats to extract
        """
        import grass.script.core as gcore
        self.outextr = "{ma}_errors".format(ma=self.intemp)
        runcom = gcore.Popen(['v.extract', 'input={i}'.format(i=self.inouttemp),
                              'output={out}'.format(out=self.outextr),
                              'cats={ct}'.format(ct=cats)],
                             stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = runcom.communicate()

        if runcom.returncode != 0:
            raise Exception("Error running GRASS: "
                            "Error running v.extract {e}".format(e=err))
        self.outputs.append(self.outextr)

    def vexists(self, inp, lay):
        """Check if a map exists and if it has some element

        :param str inp: the name of the map to check
        :param lay: the number of layer to check
        """
        self.use_cat = True
        import grass.script as grass
        try:
            infos = self.run_category(inp, 'report')
        except grass.CalledModuleError:
            return False

        if not infos:
            output = inp + "_cat"
            self.run_category(inp,  'add',  output)
            try:
                infos = self.run_category(output, 'report')
                self.use_cat = False
            except grass.CalledModuleError:
                return False

        for line in infos.splitlines():
            if line.startswith(str(lay)):
                return True
        return False

    def run_export(self, inp, out, layer=1):
        """Run v.out.ogr for single vector

        :param str inp: the name of GRASS GIS vector to export
        :param str out: the path of the output shapefile
        :param layer: the layer number to export
        """
        import grass.script.core as gcore
        com = ['v.out.ogr', 'input={ip}'.format(ip=inp),
               'output={out}'.format(out=out),
               'layer={num}'.format(num=layer)]

        if not self.use_cat:
            com.append('-c')
        runcom = gcore.Popen(com)
        out, err = runcom.communicate()
        if runcom.returncode != 0:
            raise Exception("Error running GRASS: "
                            "Error running v.out.ogr sul layer importato "
                            "{err}".format(err=err))

    def gexport(self, prefix, remove=True):
        """Export the result of analisys

        :param str prefix: the output prefix path
        :param bool remove: remove this mapset
        """
        outpaths = []
        for mapp  in self.outputs:
            if mapp == 'overlap':
                output = "{pre}_{name}_overlaps.shp".format(pre=prefix, name=self.intemp)
                mapp = self.inouttemp
                layer = 2
            else:
                output = "{pre}_{name}.shp".format(pre=prefix, name=mapp)
                layer = 1
            if self.vexists(mapp, layer):
                self.run_export(inp=mapp, out=output, layer=layer)
                outpaths.append(output)
        if remove:
            self._removeMapset()
        return outpaths
