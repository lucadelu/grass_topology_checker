# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 12:24:52 2015

@author: lucadelu
"""

__author__ = 'Luca Delucchi'
__date__ = 'October 2015'
__copyright__ = '(C) 2015 Luca Delucchi'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *

from topo_plugin_ui import Ui_GrassTopologicalCheckUI
import os
import sys
from types import StringType, UnicodeType
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from topo_plugin_settings import MessageHandler, GTCSettings
from topo_plugin_grass import gtcGRASS
import traceback
import glob
import topo_plugin_rc
import osgeo.ogr as ogr


class UiTopoCheckDialog(QDialog):
    """The dialog for the plugin"""
    def __init__(self):
        QDialog.__init__(self)
        # Set up the user interface from Designer.
        self.gui = Ui_GrassTopologicalCheckUI()
        self.gui.setupUi(self)


class TopoCheck:
    """This is the main function of the plugin. The class it is used into
    __init_.py file"""
    def __init__(self, iface):
        self.iface = iface
        self.mapcanvas = self.iface.mapCanvas()
        self.registry = QgsMapLayerRegistry.instance()

    def _check(self, string):
        """Check the type of string

        :param obj string: a string, it should be as UnicodeType or StringType
        """
        if isinstance(string, UnicodeType) or isinstance(string, StringType):
            return str(string)
        else:
            return str("")

    def initGui(self):
        """Method to add the plugin to QGIS"""
        self.action = QAction(QIcon(os.path.join(os.path.dirname(__file__),
                                                 "logo.png")),
                              "GrassTopologicalCheck", self.iface.mainWindow())
        self.action.setWhatsThis("GrassTopologicalCheck plugin")
        QObject.connect(self.action, SIGNAL("triggered()"), self.run)
        # add plugin to the web plugin menu
        self.iface.addPluginToVectorMenu("&GrassTopologicalCheck", self.action)
        # and add button to the Web panel
        self.iface.addToolBarIcon(self.action)

    def unload(self):
        """Method to unload the plugin to QGIS"""
        # new menu used, remove submenus from main Web menu
        self.iface.removePluginVectorMenu("&GrassTopologicalCheck",
                                          self.action)
        # also remove button from Web toolbar
        self.iface.removeToolBarIcon(self.action)

    def _loadSettings(self):
        """Load parameters from settings"""
        self.dlg.gui.lineEdit_grassbin.setText(self._check(GTCSettings.value("grassbin", "")))
        self.dlg.gui.lineEdit_grassdata.setText(self._check(GTCSettings.value("grassdata", "")))
        self.dlg.gui.lineEdit_location.setText(self._check(GTCSettings.value("grasslocation", "")))
        self.dlg.gui.lineEdit_epsg.setText(self._check(GTCSettings.value("epsgcode", "")))

    def _saveSettings(self):
        """Save parameters into settings"""
        GTCSettings.setValue("grassbin", self.dlg.gui.lineEdit_grassbin.text())
        GTCSettings.setValue("grassdata",
                             self.dlg.gui.lineEdit_grassdata.text())
        GTCSettings.setValue("grasslocation",
                             self.dlg.gui.lineEdit_location.text())
        GTCSettings.setValue("epsgcode", self.dlg.gui.lineEdit_epsg.text())

    def run(self):
        """Method before load the plugin"""
        self.dlg = UiTopoCheckDialog()
        self.vectors = {}
        self.vectors_qgis = {}
        self._loadSettings()
        for i in range(self.mapcanvas.layerCount()-1, -1, -1):
            layer = self.mapcanvas.layer(i)
            # check if is a vector
            if layer.type() == layer.VectorLayer:
                name = layer.name()
                source = layer.source()
                provider = layer.dataProvider().name()
                if provider == 'postgres':
                    pgsource = QgsDataSourceURI(source)
                    source = "PG:dbname='{dname}' host='{host}'" \
                             " port='{port}' user='{us}' " \
                             "password='{pwd}' table='{ta}'" \
                             "".format(dname=pgsource.database(),
                                       host=pgsource.host(),
                                       port=pgsource.port(),
                                       us=pgsource.username(),
                                       pwd=pgsource.password(),
                                       ta=pgsource.table())
                self.vectors[name] = source
                self.vectors_qgis[source] = layer
                self.dlg.gui.LayerList.addItem(name)
        if len(self.vectors) > 0:
            self.dlg.gui.progressBar.setMinimum(0)
            self.dlg.gui.progressBar.setMaximum(len(self.vectors))
        else:
            self.dlg.gui.progressBar.setValue(0)
        self.dlg.gui.LayerList.currentIndexChanged.connect(self._indexChanged)
        QObject.connect(self.dlg.gui.tabWidget, SIGNAL('currentChanged(int)'),
                        self._tabChanged)
        # button for input data
        QObject.connect(self.dlg.gui.pushButton_input, SIGNAL("clicked()"),
                        self._inputButton)
        # button for output data
        QObject.connect(self.dlg.gui.pushButton_output, SIGNAL("clicked()"),
                        self._outputButton)
        # button for grassdata
        QObject.connect(self.dlg.gui.pushButton_grassbin, SIGNAL("clicked()"),
                        self._grassbin)
        # button for grassdata
        QObject.connect(self.dlg.gui.pushButton_grassdata, SIGNAL("clicked()"),
                        self._grassdata)
        # button for start the plugin
        QObject.connect(self.dlg.gui.buttonBox, SIGNAL("accepted()"),
                        self.convert)
        # button for close the plugin after create openlayers file
        QObject.connect(self.dlg.gui.buttonBox, SIGNAL("rejected()"),
                        self.dlg.close)
        self._indexChanged()
        self.dlg.show()

    def _grassbin(self):
        """Function called to selct GRASS GIS binary path"""
        mydir = QFileDialog.getOpenFileName(parent=None, directory="",
                                            caption="Select GRASS binary",
                                            options=QFileDialog.DontUseNativeDialog)
        if os.path.exists(mydir):
            self.dlg.gui.lineEdit_grassbin.setText(mydir)
            return
        else:
            MessageHandler.error("The GRASS binary {name} does not "
                                 "exists".format(name=mydir))

    def _grassdata(self):
        """Function called to selct GRASS GIS GRASSDATA"""
        label = "Select the GRASSDATA directory"
        mydir = QFileDialog.getExistingDirectory(parent=None, caption=label,
                                                 directory="",
                                                 options=QFileDialog.DontUseNativeDialog)
        if os.path.exists(mydir):
            self.dlg.gui.lineEdit_grassdata.setText(mydir)
            return
        else:
            MessageHandler.error("The directory {name} does not "
                                 "exists".format(name=mydir))

    def _tabChanged(self, selected_index):
        """Function called when tab are changed"""
        if selected_index == 0 or selected_index == 2:
            self._saveSettings()
        else:
            self._loadSettings()

    def _indexChanged(self):
        """Add source to the line edit"""
        try:
            self.dlg.gui.lineEdit_input.setText(self.vectors[self.dlg.gui.LayerList.currentText()])
        except Exception:
            pass

    def _inputButton(self):
        """Function called with input button"""
        mydir = QFileDialog.getOpenFileName(parent=None, directory="",
                                            caption="Select the input data",
                                            options=QFileDialog.DontUseNativeDialog)
        if os.path.exists(mydir):
            self.dlg.gui.lineEdit_input.setText(mydir)
            try:
                inp = ogr.Open(mydir)
                lay = inp.GetLayer()
                self.vectors[lay.GetName()] = inp.GetName()
                self.dlg.gui.progressBar.setMinimum(0)
                self.dlg.gui.progressBar.setMaximum(len(self.vectors))
            except:
                MessageHandler.error("The input file doesn't seem vector data")
            return

    def _outputButton(self):
        """Function called with output button"""
        mydir = name = QFileDialog.getSaveFileName(parent=None,
                                                   directory="",
                                                   caption="Select the "
                                                           "prefix for "
                                                           "output data",
                                                   options=QFileDialog.DontUseNativeDialog)
        if not os.path.exists(mydir):
            self.dlg.gui.lineEdit_output.setText(mydir)
            return
        else:
            MessageHandler.error("The output files already exist,")

    def _remExt(self):
        """Remove extension from output is present"""
        exts = ['.shp', '.gml', '.kml', '.json', '.geojson']
        out = str(self.dlg.gui.lineEdit_output.text())
        for ext in exts:
            if ext in out:
                return out.replace(ext, '')
            elif ext.upper() in out:
                return out.replace(ext.upper(), '')
            else:
                return out

    def _addLayer(self, filename):
        """Add output layer into QGIS map canvas"""
        registry = QgsMapLayerRegistry.instance()
        layer = QFileInfo(filename)
        layerName = layer.baseName()
        err = "Problem loading layer {na}, the layer" \
              "could be written erroneously".format(na=layerName)
        if not layer.exists():
            MessageHandler.warning(err)
        layer = QgsVectorLayer(filename, layerName, 'ogr')
        if not layer.isValid():
            MessageHandler.error(err)
        else:
            registry.addMapLayer(layer)

    def _outExist(self, out):
        """Check if the output already exists

        :param str out: the output path
        """
        res = QMessageBox.question(None, "Grass Topological Check Plugin",
                                   "{0} already exists. Replace it?".format(out),
                                   QMessageBox.Yes | QMessageBox.No,
                                   QMessageBox.No)
        if res == QMessageBox.Yes:
            outs = os.path.split(out)
            pref = outs[1].replace('.shp', '')
            files = glob.glob1(outs[0], "{pr}*".format(pr=pref))
            for fil in files:
                os.remove(os.path.join(outs[0], fil))
            return 0
        elif res == QMessageBox.No:
            return 1

    def convert(self):
        """Method to run the plugin to QGIS"""
        grassbin = str(GTCSettings.value("grassbin", ""))
        grassdatabase = str(GTCSettings.value("grassdata", ""))
        location = str(GTCSettings.value("grasslocation", ""))
        epsg = str(GTCSettings.value("epsgcode", ""))
        if not grassbin or not grassdatabase or not location or not epsg:
            MessageHandler.error("Please set the needed information in the "
                                 "'Settings' tabs.")
        self._saveSettings()
        try:
            inp = str(self.dlg.gui.lineEdit_input.text())
            qgislay = self.vectors_qgis[inp]
            snap = str(self.dlg.gui.lineEdit_snap.text())
            if not snap:
                snap = '-1'
            outpath = self._remExt()
            outogrlog = "{pre}_{su}.log".format(pre=outpath,
                                                su='shp2grass_checker')
            grass = gtcGRASS()
            grass.gimport(inp, snap, self.dlg.gui.overwrite.isChecked(),
                          outogrlog)
            grass.run_build()
            allcats = grass.run_category()

#            from PyQt4.QtCore import *
#            import pdb
#            pyqtRemoveInputHook()
#            pdb.set_trace()
            cats = set([x for x in allcats if allcats.count(x) > 1])
            if len(cats) > 0:
                grass.run_extract(','.join(cats))

            if qgislay.geometryType() == 1:
                grass.run_clean(outogrlog, 'break')
            outs = grass.gexport(outpath)
            for out in outs:
                self._addLayer(out)
            MessageHandler.success("Done")
        except:
            error = traceback.format_exc()
            MessageHandler.error(error)
            return
